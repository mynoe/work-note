> 博文强记，开卷有益。

### 前情回顾
上篇文章大致讲了`vim`及`vim`的基本用法，这里简单说`nginx`的基本z知识点。

###  nginx是什么

`Nginx (engine x)` 是一个高性能的HTTP和反向代理web服务器，同时也提供了IMAP/POP3/SMTP服务。


### nginx 的启动 停止 测试 重启 
- 启动 
```bash
# !/bin/bash
./sbin/Nginx 
```
- 停止 
```bash
# !/bin/bash
./sbin/Nginx  -g TERM | INT | QUIT
# TERM | INT 用于快速停止
# QUIT 用于平缓停止
```
- 测试
```bash
# !/bin/bash
./sbin/Nginx -t

# -t  用于测试nginx配置是否正确
```

- 重启
```bash
# !/bin/bash
./sbin/Nginx -g HUP

# HUP  用于发送平滑重启信号
```

以上命令是直接执行的二进制文件，有的版本也可以执行命令`nginx server start` 或 `nginx service restart`命令去进行启停。


### nginx.conf配置文件
`nginx`的原始配置在`nginx.conf`文件中，代码清单如下：
```bash
worker_process 1;  # 全局生效

events {
  worker_connections 1024;  # events部分中生效
}

http {
  include  mime.types;
  default_type application/octet-stream;
  sendfile on;
  keepalive_timeout 65;
  server {
    listen  80;
    server_name localhost;
    location / {
      root html;
      index index.htm index.html;
    }
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
      root html;
    }
  }
  
}
```
### 常见的几个需要配置的地方
- `server_name`
```bash
server_name name;
# name 支持正则表达式 如：
# server_name ~^www\d+\.myserver\.com$;
```
- `location`
```bash
# 语法为
location [ = | ~ | ~* | ^~ ] uri {...}
# = 表示与 uri 严格匹配
# ~ 表示 uri 包含正则 且 区分大小写
# ~* 表示 uri 包含正则 且 不区分大小写
# ^~ 表示 找到与 uri 匹配度最高的location 后进行请求处理
```
- `根目录配置`
```bash
root path;
# path 是服务器接收到请求后查找资源的根目录
```
- `基于IP配置访问权限`
```bash
# 指令 allow 用于设置允许访问nginx的客户端IP 语法：
# allow address | CIDR | all;
# 指令 deny 作用想反
# 配置demo:

location / {
  deny 192.168.1.1;
  allow 192.168.1.0/24;
  deny all;
}
```
### nginx 与 防盗链
最近有点忙，不写那么多东西了，最后说一下防盗链的原理。要实现`防盗链`需要了解请求头中的`Referer`头域和采用的url格式表示当前的文件地址。通过这个域，我们可以检测到访问目标的原地址。如果我们检测到不是自己站点内的URL,就采取阻止措施。

知道了原理，我们就可以用nginx的`rewrite`功能实现防盗链。nginx配置中有个`valid_referers`用来获取`Referer`头域中的值，并且根据情况对全局变量`$valid_referers`赋值， 如果符合，则赋值为1;
语法如下：
```bash
valid_referers none | blocked | server_name 
# none 表示 检测域头不存在的情况
# blocked 表示 域头被防火墙删除的情况
```
根据文件类型实现防盗链的配置demo:
```bash
# 如果域头中没有符合 valid_referers 指令，则重定向
server {
  listen 80;
  server_name www.test.com;
  location ~* ^.+\.(gif|png|jpg)$ {
    valid_referers none blocked server_names *.test.com;
    if($valid_referers){
      rewrite ^/ http://www.test.com/images/404.png;
    }
  }
}
```


###  今日总结

- nginx 的启停
- 防盗链的原理及配置

###  最后说两句
1. 动一动您发财的小手，**`「点个赞吧」`**
2. 动一动您发财的小手，**`「点个在看」`**
3. 都看到这里了，不妨  **`「加个关注」`** 

![javascript基础知识总结](https://img2020.cnblogs.com/blog/986934/202012/986934-20201230192917500-205455520.png)