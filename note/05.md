>大太监曹少钦，自认东厂督公，上挟天子，下令百官，凡有敢于作对者，皆酷刑致死，世人说起东厂皆谈虎色变。
---《新龙门客栈》

### 抛开业务谈技术是不是在耍流氓？

### vuex的state数据到底存在什么地方？
存在Vue的一个实例上
```javascript
class Store{
  constructor({
    state ={},
    mutation ={},
    modules = {},
    plugins = [],
    strict = false,
  }= {}) {
    this._getterCacheId = 'vuex_store_' + uid++
    this._dispatching = false
    this._rootMutations = this._mutations = mutations
    this._modules = modules
    this._subscribers = []
    // 用一个vue实例储存状态树
    this._vm = new Vue({
      data:{
        state
      }
    })
    
    // 然后设置模块儿的State
    this._setupModuleState(state,modules)
    // 设置module的Mutation
    this._setupModuleMutations(modules)
  }
  
  // 通过disapatch方法触发action
  dispatch(type,...payload){
    // 还是
  }

}