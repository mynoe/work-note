>> 有志者，事竟成，百二秦关终属楚。苦心人，天不负，三千越甲可吞吴。

### 前情回顾


Vue的其中一个核心就是`虚拟Dom`,虚拟Dom的核心是`vnode`，所以想弄清楚vnode , component, elelement之前的关系，还是先要知道vnode到底是个什么东西。

### Vnode接口定义
源码中`vnode.d.ts`文件中定义了`vnode`的接口,以及相关的其他接口，在具体的框架实现中，可以根据需要对这些接口进行具体实现。

```javascript
//  Vnode接口
export interface VNode {
  tag?: string;  // 标签
  data?: VNodeData; // 数据
  children?: VNode[]; // childern
  text?: string; // 文本
  elm?: Node;  // 元素
  ns?: string; // namespace 命名空间
  context?: Vue; // 上下文
  key?: string | number;  // key 
  componentOptions?: VNodeComponentOptions;
  componentInstance?: Vue;
  parent?: VNode;
  raw?: boolean;
  isStatic?: boolean;
  isRootInsert: boolean;
  isComment: boolean;
}
/**
VNodeChildrenArrayContents
**/
export interface VNodeChildrenArrayContents {
  [x: number]: VNode | string | VNodeChildren;
}

export type VNodeChildren = VNodeChildrenArrayContents | [ScopedSlot] | string;

export type ScopedSlot = (props: any) => VNodeChildrenArrayContents | string;

/**
  VNodeComponentOptions
**/ 
export interface VNodeComponentOptions {
  Ctor: typeof Vue;
  propsData?: Object;
  listeners?: Object;
  children?: VNodeChildren;
  tag?: string;
}
/**
  VNodeData 数据结构
  看到这个结构，可以想到在使用render函数时，
  传的style,props,on等属性
**/
export interface VNodeData {
  key?: string | number;
  slot?: string;
  scopedSlots?: { [key: string]: ScopedSlot };
  ref?: string;
  tag?: string;
  staticClass?: string;
  class?: any;
  staticStyle?: { [key: string]: any };
  style?: Object[] | Object;
  props?: { [key: string]: any };
  attrs?: { [key: string]: any };
  domProps?: { [key: string]: any };
  hook?: { [key: string]: Function };
  on?: { [key: string]: Function | Function[] };
  nativeOn?: { [key: string]: Function | Function[] };
  transition?: Object;
  show?: boolean;
  inlineTemplate?: {
    render: Function;
    staticRenderFns: Function[];
  };
  directives?: VNodeDirective[];
  keepAlive?: boolean;
}

```
大体上讲，vnode 接受tag标签名，data数据(其中data又包括class样式，style样式对象，props对象，atrr属性，on事件等),children(同样是vnode类型)，parent(vnode类型)等作为参数。


### Component相关接口
- `函数组件`
```javascript
export interface FunctionalComponentOptions<Props = DefaultProps, PropDefs = PropsDefinition<Props>> {
  name?: string;
  props?: PropDefs;
  inject?: InjectOptions;
  functional: boolean;
  render(this: undefined, createElement: CreateElement, context: RenderContext<Props>): VNode;
}


```
 `这里需要注意，组件接受name,props及render函数作为参数，render函数返回值为vnode。
 并且，render具体是什么，其实就是createElement方法。`
 - `正常的Component`

```javascript
export interface ComponentOptions<
  V extends Vue,
  Data=DefaultData<V>,
  Methods=DefaultMethods<V>,
  Computed=DefaultComputed,
  PropsDef=PropsDefinition<DefaultProps>> {
  // 这里是Component中的属性
  data?: Data;
  props?: PropsDef;
  propsData?: Object;
  computed?: Accessors<Computed>;
  methods?: Methods;
  watch?: Record<string, WatchOptionsWithHandler<any> | WatchHandler<any> | string>;

  el?: Element | String;
  template?: string;
  render?(createElement: CreateElement): VNode;
  renderError?: (h: () => VNode, err: Error) => VNode;
  staticRenderFns?: ((createElement: CreateElement) => VNode)[];
  
// 这里是生命周期
  beforeCreate?(this: V): void;
  created?(): void;
  beforeDestroy?(): void;
  destroyed?(): void;
  beforeMount?(): void;
  mounted?(): void;
  beforeUpdate?(): void;
  updated?(): void;
  activated?(): void;
  deactivated?(): void;
  errorCaptured?(): boolean | void;

  directives?: { [key: string]: DirectiveFunction | DirectiveOptions };
  components?: { [key: string]: Component<any, any, any, any> | AsyncComponent<any, any, any, any> };
  transitions?: { [key: string]: Object };
  filters?: { [key: string]: Function };

  provide?: Object | (() => Object);
  inject?: InjectOptions;

  model?: {
    prop?: string;
    event?: string;
  };

  parent?: Vue;
  mixins?: (ComponentOptions<Vue> | typeof Vue)[];
  name?: string;
  // TODO: support properly inferred 'extends'
  extends?: ComponentOptions<Vue> | typeof Vue;
  delimiters?: [string, string];
  comments?: boolean;
  inheritAttrs?: boolean;
}
```
`可以看到，组件接口中有我们常用的data,props,computed,methods,watch,及生命周期函数，以及其他的一些属性`

### `vnode`,`component`,`element`关系体现
在`create-component.js`文件中有`createComponent`函数，该函数最终返回值为`vnode`。
```javascript
export function createComponent (
  Ctor: Class<Component> | Function | Object | void,
  data: ?VNodeData,
  context: Component,
  children: ?Array<VNode>,
  tag?: string
): VNode | void {
// other code 
}
```
在`create-element.js`文件中有`_createElement`函数，该函数最终返回值为`vnode`。
```javascript
  export function _createElement (
  context: Component,
  tag?: string | Class<Component> | Function | Object,
  data?: VNodeData,
  children?: any,
  normalizationType?: number
): VNode {
  // other code
}
```
所以，无论是Component还是Element最终都会被解析成vnode,然后将这个vnode重新解析为html进行渲染。

### 问题

HTML 如何解析成 VNODE , VNODE 又如何解析成HTML?

###  最后说两句
1. 动一动您的小手，**`「点个赞吧」`**
2. 都看到这里了，不妨  **`「加个关注」`** 

![javascript基础知识总结](https://img2020.cnblogs.com/blog/986934/202012/986934-20201230192917500-205455520.png)