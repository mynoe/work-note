>十年生死两茫茫，不思量，自难忘，千里孤坟无处话凄凉。纵使相逢应不识，尘满面，鬓如霜，相顾无言唯有泪千行。

### 前情回顾
上篇文章大致讲了eventMixin的实现方法。本事上是一个发布订阅模式，在`Vue`的原型上添加了`$on`,`$off`,`$once`,`$emit`四个对外暴露的API方法。

今天我们来看 `stateMixin(Vue)`这个方法。这个方法定义在`src/instance/state.js`中，在`src/instance/index.js`中被调用。
```javascript

// src/instance/state.js
function Vue(options){

  this._init(options)
}
  // init相关
  initMixin(vue)
  // 事件相关
  eventsMixin(Vue)
  // 
  stateMixin(Vue)
  
  // state.js
  
```
### stateMixin详解
`stateMixin(Vue)`方法，在`Vue`的原型上添加了`$data`,`$props`,`$set`,`$delete`,`$watch`,这个几个方法，暴露为对外API。其中`$data`,`$props`为实例属性。`$set`,`$delete`,`$watch`为实例方法。这个可以在官方文档中查阅。

下面我们看下代码实现:

```javascript
  export function stateMixin (Vue: Class<Component>) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  const dataDef = {}
  dataDef.get = function () { return this._data }
  const propsDef = {}
  propsDef.get = function () { return this._props }
  if (process.env.NODE_ENV !== 'production') {
    dataDef.set = function (newData: Object) {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      )
    }
    propsDef.set = function () {
      warn(`$props is readonly.`, this)
    }
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef)
  Object.defineProperty(Vue.prototype, '$props', propsDef)

  Vue.prototype.$set = set
  Vue.prototype.$delete = del

  Vue.prototype.$watch = function (
    expOrFn: string | Function,
    cb: any,
    options?: Object
  ): Function {
    const vm: Component = this
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {}
    options.user = true
    const watcher = new Watcher(vm, expOrFn, cb, options)
    if (options.immediate) {
      cb.call(vm, watcher.value)
    }
    return function unwatchFn () {
      watcher.teardown()
    }
  }
}
```
`dataDef` 及`propsDef`这个两个对象，分别定义了`get`和`set`方法。然后通过`Object.defineProperty`方法，在`Vue.prototype`上添加了`$data`,`$props`属性，同时定义的`get,set`对这两个数据做了拦截,保证了`props`是只读的。
然后直接在 `Vue.prototype`上添加了两个方法，`$set`和`$delete`。这两个方法的具体实现如下：

- `$set`这个方法接受三个参数，`target`,`key`,`val`。当target为数组时，在对应的位置更新数组的值。当target为对象时，更新对应key的值。
```javascript
/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
export function set (target: Array<any> | Object, key: any, val: any): any {
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key)
    target.splice(key, 1, val)
    return val
  }
  if (hasOwn(target, key)) {
    target[key] = val
    return val
  }
  const ob = (target: any).__ob__
  if (target._isVue || (ob && ob.vmCount)) {
    process.env.NODE_ENV !== 'production' && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    )
    return val
  }
  if (!ob) {
    target[key] = val
    return val
  }
  defineReactive(ob.value, key, val)
  ob.dep.notify()
  return val
}
```
- `delete`跟`set`相反，是删除对应位置的值。
```javascript
export function del (target: Array<any> | Object, key: any) {
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1)
    return
  }
  const ob = (target: any).__ob__
  if (target._isVue || (ob && ob.vmCount)) {
    process.env.NODE_ENV !== 'production' && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    )
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key]
  if (!ob) {
    return
  }
  ob.dep.notify()
}
```
`set`,`delete`也是Vue全局的API,文档上也都可以查阅。

### 需要花大段篇幅讲的问题
- `Dep`是什么？
- `Watcher`是什么？
- `observer`是什么？

在刚才说的`get`,`delete`,以及 `Vue.prototype.$watch`方法中其实都包含了这三个问题，这个需要仔细的去深入理解，接下来的一两篇文章会将这个几个问题解释清楚。


###  今日总结
`stateMixin`方法还是在`Vue.prototype`上添加了全局的API,但是已经涉及到一些核心的概念。接下来需要花点时间去仔细思考，深入理解了。

###  最后说两句
1. 动一动您发财的小手，**`「点个赞吧」`**
2. 动一动您发财的小手，**`「点个在看」`**
3. 都看到这里了，不妨  **`「加个关注」`** 

![javascript基础知识总结](https://img2020.cnblogs.com/blog/986934/202012/986934-20201230192917500-205455520.png)