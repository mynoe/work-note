> 为中华民族的伟大复兴而读书。

### 前情回顾


上篇文章分享了的一个`实现基于git hooks的持续集成`的问题，我们已经可以简单的利用`git`的钩子实现持续集成，那么小程序是否也可以实现持续集成呢，今天我们要聊一个这个问题。

### 目前发布小程序现状

在小程序开发过程中，大多数的开发者开完成后，都是通过开发者工具进行编译、预览、上传，然后登录小程序后台，进行发布。

可能很少人关注小程序的CI辅助工具，`miniprogram-ci`。



### `miniprogram-ci`是什么
`miniprogram-ci` 是从微信开发者工具中抽离的关于小程序/小游戏项目代码的编译模块。

开发者可不打开小程序开发者工具，独立使用 miniprogram-ci 进行小程序代码的上传、预览等操作。

### `miniprogram-ci`能做什么

- `上传代码`，对应小程序开发者工具的上传
- `预览代码`，对应小程序开发者工具的预览
- `构建 npm`，对应小程序开发者工具的: 菜单-工具-构建npm
- `上传云开发云函数代码`，对应小程序开发者工具的上传云函数能力
- `代理`，配置 miniprogram-ci 的网络请求代理方式
- `支持获取最近上传版本的 sourceMap`
- `支持 node 脚本调用方式和 命令行 调用方式`

### 如何用`miniprogram-ci`上传代码
- 第一，需要到微信公众平台配置`密钥及 IP 白名单`。

  使用 miniprogram-ci 前应访问"微信公众平台-开发-开发设置"后下载代码上传密钥，并配置 IP 白名单 开发者可选择打开 IP 白名单，打开后只有白名单中的 IP 才能调用相关接口。我们建议所有开发者默认开启这个选项，降低风险 代码上传密钥拥有预览、上传代码的权限，密钥不会明文存储在微信公众平台上，一旦遗失必须重置，请开发者妥善保管
  ![密钥及 IP 白名单](https://res.wx.qq.com/wxdoc/dist/assets/img/ci-manager.e520fdd9.png)
- 第二，在项目中安装`miniprogram-ci`的npm 包
  ```
  npm install miniprogram-ci --save
  ```
- 第三，创建文件编写上传代码。

  写这个代码之前需要明白`项目对象`的概念。可以理解它是对整个小程序的一种抽象，定义如下：

  ```javascript
  interface IProject {
    appid: string // 小程序/小游戏项目的 appid
    type: string // 项目的类型，有效值 miniProgram/miniProgramPlugin/miniGame/miniGamePlugin
    projectPath: string //项目的路径，即 project.config.json 所在的目录
    privateKey: string //私钥，在获取项目属性和上传时用于鉴权使用，在 微信公众平台 上登录后下载 
    attr(): Promise<IProjectAttr> // 项目的属性，如指定了 privateKey 则会使用真实的项目属性
    stat(prefix: string, filePath: string): IStat | undefined // 特定目录下前缀下（prefix）文件路径 (filePath) 的 stat, 如果不存在则返回 undefined
    getFile(prefix: string, filePath: string): Promise<Buffer> // 特定目录下前缀下（prefix）文件路径 (filePath) 的 Buffer
    getFileList(prefix: string, extName: string): string[] // 特定目录下前缀下（prefix）文件路径 (filePath) 下的文件列表
    updateFiles: () => void // 更新项目文件 
  }

  ```
  
  然后就可以来实现一个上传的功能了，比如官方文档上的代码：

  ```javascript
  // upload.js

  // 引用 miniprogram-ci
  
  const ci = require('miniprogram-ci')
  ;(async () => {
    //  创建项目对象
    const project = new ci.Project({
      appid: 'wxsomeappid',
      type: 'miniProgram',
      projectPath: 'the/project/path',
      privateKeyPath: 'the/path/to/privatekey',
      ignores: ['node_modules/**/*'],
    })
    // 执行传动作
    const uploadResult = await ci.upload({
      project,
      version: '1.1.1',
      desc: 'hello',
      setting: {
        es6: true,
      },
      onProgressUpdate: console.log,
    })
    // 打印上传结果
    console.log(uploadResult)
  })()

  ```
  `我们可以将这个代码当成一个命令放到package.json文件中`
  ```javascript
  // package.json
  {
    "name":"miniProgram",
    "version":"1.0.0",
    "scripts":{
      "npm run upload": "node ./upload.js"
    }
  }
    
  ```
  这样我们本地开发完成后执行`npm run upload`即可完成小程序的上传工作。

  `预览功能`同理。

  ### 实现小程序的持续集成

  结合上一篇文章聊过的实现持续集成的方案，我们只需要在`git hooks`的`post-update`钩子中，配置上对应小程序的`上传`脚本即可实现小程的持续集成。

  `有可能需要考虑的问题`---> 在预览时能够显示预览的二维码。


### 总结
- 需要知道`miniprogram-ci`包
- 需要知道`miniprogram-ci`包，对外提供的api
  - `upload` 上传
  - `preview` 预览
  - `packNpm` 构建npm
  - `getDevSourceMap` 拉取最近上传版本的sourceMap

###  最后说两句
1. 动一动您发财的小手，**`「点个赞吧」`**
2. 动一动您发财的小手，**`「点个在看」`**
3. 都看到这里了，不妨  **`「加个关注」`**
3. 不妨  **`「转发一下」`**，好东西要记得分享

![javascript基础知识总结](https://blog-static.cnblogs.com/files/vali/my_gif_qrcode.gif)
